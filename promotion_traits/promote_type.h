#ifndef __PROMOTE_TYPE_H_
#define __PROMOTE_TYPE_H_

/*
 *  example traits for type promotions
 */

// starting with the basic template
//template<class T1, class T2>
//struct PromotionTraits
//{};

// default is to promote to type T1
template<class T1, class T2>
struct PromotionTraits
{
    typedef T1 promoted_type;
};

// nothing to do
template<class T>
struct PromotionTraits<T,T>
{
    typedef T promoted_type;
};

// specialisation for promoting int --> double
template<>
struct PromotionTraits<double, int>
{
    typedef double promoted_type;
};

template<>
struct PromotionTraits<int, double>
{
    typedef double promoted_type;
};

#endif
