#include<cmath>
#include<math.h>
#include "nummatrix.h"

template<typename T>
class Norm
{
  public:
    Norm() {}
    
    virtual double operator() (NumMatrixClass<T> const &a) = 0;

};

template<typename T>
class MaxColumnSumNorm : public Norm<T>
{
  public:
    MaxColumnSumNorm() : Norm<T>() {}
  
    double operator() (NumMatrixClass<T> const &a)
    {
      double max = 0.0;
      for(int j=0; j<a.Cols(); ++j)
      {
        double colSum = 0.0;
        for(int i=0; i<a.Rows(); ++i)
        {
          colSum += std::abs(a(i,j));
        }
        if(colSum > max) 
          max = colSum;
      }
      return max;
    }
};

template<typename T>
class FrobeniusNorm : public Norm<T>
{
  public:
  
    double operator() (NumMatrixClass<T> const &a)
    {
      double sum = 0.0;
      for(int i=0; i<a.Rows(); ++i)
      {
        for(int j=0; j<a.Cols(); ++j)
        {
          sum += a(i,j)*a(i,j);
        }
      }
      return std::sqrt(sum);
    }
};

template<typename T>
class MaxRowSumNorm : public Norm<T>
{
  public:
    MaxRowSumNorm() : Norm<T>() {}
  
    T operator() (NumMatrixClass<T> const &a)
    {
      double max = 0.0;
      for(int i=0; i<a.Rows(); ++i)
      {
        double rowSum = 0.0;
        for(int j=0; j<a.Cols(); ++j)
        {
          rowSum += std::abs(a(i,j));
        }
        if(rowSum > max) 
          max = rowSum;
      }
      return max;
    }
};
