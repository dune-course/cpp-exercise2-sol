/**
 *  Exercise 3 - Part II
 *
 *  Numeric matrices NumMatrixClass
 */

#include <math.h>
#include "nonnummatrix.h"


template<class T>
class NumMatrixClass : public MatrixClass<T>
{
  public:
    
    // Multiplication by a value x
    NumMatrixClass &operator*=(T x);
    
    // Addition with another matrix
    NumMatrixClass &operator+=(const NumMatrixClass &b);
    
    // Constructors
    NumMatrixClass(int numRows, int numCols) : 
                   MatrixClass<T>(numRows,numCols) {}
    
    NumMatrixClass(int numRows, int numCols, T value) : 
                   MatrixClass<T>(numRows,numCols,value) {}
    
};
    

// Multiplication by a value x
template<class T>
NumMatrixClass<T> &NumMatrixClass<T>::operator*=(T x)
{
    for (int i=0; i<this->numRows_; ++i)
        for (int j=0; j<this->numCols_; ++j)
            this->a_[i][j] *= x;
    return *this;
}

// Addition with another matrix
template<class T>
NumMatrixClass<T> &NumMatrixClass<T>::operator+=(const NumMatrixClass<T> &x)
{
    if ((x.numRows_!=this->numRows_)||(x.numCols_!=this->numCols_))
    {
        std::cerr << "Dimensions of matrix a (" << this->numRows_
                  << "x" << this->numCols_ << ") and matrix x ("
                  << this->numRows_ << "x" << this->numCols_ << ") do not match!";
        exit(EXIT_FAILURE);
    }
    for (int i=0;i<this->numRows_;++i)
        for (int j=0;j<x.numCols_;++j)
            this->a_[i][j]+=x(i,j);
    return *this;
}

// More arithmetic operators
template<class T>
NumMatrixClass<T> operator*(const NumMatrixClass<T> &a, T x)
{
    NumMatrixClass<T> temp(a);
    temp *= x;
    return temp;
}

template<class T>
NumMatrixClass<T> operator*(T x, const NumMatrixClass<T> &a)
{
    NumMatrixClass<T> temp(a);
    temp *= x;
    return temp;
}

template<class T>
NumMatrixClass<T> operator+(const NumMatrixClass<T> &a,const NumMatrixClass<T> &b)
{
    NumMatrixClass<T> temp(a);
    temp += b;
    return temp;
}

