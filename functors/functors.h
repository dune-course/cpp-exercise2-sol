#include<cmath>
#include<math.h>
#include "nummatrix.h"


class MaxColumnSumNorm
{
  public:     
    template<typename T>
    double operator() (NumMatrixClass<T> const &a)
    {
      double max = 0.0;
      for(int j=0; j<a.Cols(); ++j)
      {
        double colSum = 0.0;
        for(int i=0; i<a.Rows(); ++i)
        {
          colSum += std::abs(a(i,j));
        }
        if(colSum > max) 
          max = colSum;
      }
      return max;
    }
};

class FrobeniusNorm
{
  public:
    template<typename T>
    double operator() (NumMatrixClass<T> const &a)
    {
      double sum = 0.0;
      for(int i=0; i<a.Rows(); ++i)
      {
        for(int j=0; j<a.Cols(); ++j)
        {
          sum += a(i,j)*a(i,j);
        }
      }
      return std::sqrt(sum);
    }
};

class MaxRowSumNorm
{
  public: 
    template<typename T> 
    double operator() (NumMatrixClass<T> const &a)
    {
      double max = 0.0;
      for(int i=0; i<a.Rows(); ++i)
      {
        double rowSum = 0.0;
        for(int j=0; j<a.Cols(); ++j)
        {
          rowSum += std::abs(a(i,j));
        }
        if(rowSum > max) 
          max = rowSum;
      }
      return max;
    }
};

class AverageNorm
{
  public:
    AverageNorm()
    : sum(0.0), nElements(0)
    {}
    
       
    double operator() (double const &a)
    {
      sum += a;
      nElements++;
      
      return (sum/nElements);
    }
    
    void reset()
    {
      sum = 0.0;
      nElements = 0;
    }
    
  private:
    double sum;
    int nElements;
};
