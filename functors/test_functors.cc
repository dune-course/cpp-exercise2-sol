#include <cstdlib>
#include <iostream>
#include <complex>
#include <iomanip>

#include "functors.h"

using namespace std;


//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    // define some matrices
    //=====================

    // double 4x3
    NumMatrixClass<double> A(4,3);
    double start1 = 2.0;
    for (int i=0; i<A.Rows(); ++i)
      for (int j=0; j<A.Cols(); ++j)
        A(i,j) = start1 + i + j;

    NumMatrixClass<int> B(4,3);
    int start2 = 2;
    for (int i=0; i<B.Rows(); ++i)
      for (int j=0; j<B.Cols(); ++j)
        B(i,j) = start2 + i + j;

    // test computation of norms
    //==========================

    std::cout << std::endl << "========= matrix norms =========" << std::endl << std::endl;

    // define functors for different matrix norms
	 MaxColumnSumNorm n1;
	 FrobeniusNorm n2;
	 MaxRowSumNorm n3;

	 AverageNorm avg;


    // compute norm
    std::cout << "========= double matrix =========" << std::endl;
    A.Print();
    std::cout << "Column-sum norm: " << n1(A) << std::endl;
    std::cout << "Frobenius norm: " << n2(A) << std::endl;
    std::cout << "Row-sum norm: " << n3(A) << std::endl;
    avg(n1(A));
    avg(n2(A));
    std::cout << "Average norm: " << avg(n3(A)) << std::endl;

    std::cout << std::endl << "========== int matrix ===========" << std::endl;
    B.Print();
    std::cout << "Column-sum norm: " << n1(B) << std::endl;
    std::cout << "Frobenius norm: " << n2(B) << std::endl;
    std::cout << "Row-sum norm: " << n3(B) << std::endl;
    avg.reset();
	 avg(n1(B));
    avg(n2(B));
    std::cout << "Average norm: " << avg(n3(B)) << std::endl;


    return (0);
}
