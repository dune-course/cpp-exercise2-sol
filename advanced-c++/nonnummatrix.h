/**
 *  Exercise 3 - Part II
 *
 *  Templated non-numeric MatrixClass
 */

#include <vector>
#include <iostream>
#include <iomanip>

template<class T>
class MatrixClass
{
  public:

    // Set number of matrix rows and columns and 
    // initialize matrix elements with a given value
    void Resize(int numRows, int numCols, const T &value=T() );
    
    // Access matrix element at position (i,j)
    T &operator()(int i, int j);
    T operator()(int i, int j) const;
    
    // Output matrix content
    void Print() const;
    
    // Returns number of matrix raws
    int Rows() const 
    {
        return numRows_;
    }
    
    // Returns number of matrix columns
    int Cols() const
    {
        return numCols_;
    }

    // Constructors
    
    MatrixClass(int numRows=0, int numCols=0, const T &value=T() ) :
      a_(numRows), numRows_(numRows), numCols_(numCols)
    {
      if ( ((numCols==0) && (numRows!=0)) || ((numCols!=0) && (numRows==0)) )
      {
        numRows_=0;
        numCols_=0;
        a_.resize(numRows_);
      }
      else
      {
          for (int i=0;i<numRows_;++i)
              a_[i].resize(numCols_);
      }
      for (int i=0;i<numRows;++i)
      {
          for (int j=0;j<numCols;++j)
              a_[i][j]=value;
      }
    }
    
    // row iterators
    typedef typename std::vector<std::vector<T> >::iterator RowIt;
    typedef typename std::vector<std::vector<T> >::const_iterator ConstRowIt;

    // methods returning row iterators
    RowIt begin();
    RowIt end();
    ConstRowIt begin() const;
    ConstRowIt end() const;

    // column iterators
    typedef typename std::vector<T>::iterator ColIt;
    typedef typename std::vector<T>::const_iterator ConstColIt;

  protected:
    // matrix elements
    std::vector<std::vector<T> > a_;
    // number of rows
    int numRows_;
    //number of columns
    int numCols_;
};


// Set number of matrix rows and columns and 
// initialize matrix elements with a given value
template<class T>
void MatrixClass<T>::Resize(int numRows, int numCols, const T &value)
{
  a_.resize(numRows);
  for (int i=0;i<a_.size();++i)
  {
    a_[i].resize(numCols);
    for (int j=0;j<a_[i].size();++j)
        a_[i][j]=value;
  }
  numRows_=numRows;
  numCols_=numCols;
}

// Access matrix element at position (i,j)
template<class T>
T &MatrixClass<T>::operator()(int i, int j)
{
    if ((i<0)||(i>=numRows_))
    {
        std::cerr << "Illegal row index " << i;
        std::cerr << " valid range is (0:" << numRows_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    if ((j<0)||(j>=numCols_))
    {
        std::cerr << "Illegal column index " << j;
        std::cerr << " valid range is (0:" << numCols_-1 << ")";
        std::cerr << std::endl;
        exit(EXIT_FAILURE);
    }
    return a_[i][j];
}

template<class T>
T MatrixClass<T>::operator()(int i, int j) const
{
  if ((i<0)||(i>=numRows_))
  {
    std::cerr << "Illegal row index " << i;
    std::cerr << " valid range is (0:" << numRows_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  if ((j<0)||(j>=numCols_))
  {
    std::cerr << "Illegal column index " << j;
    std::cerr << " valid range is (0:" << numCols_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  return a_[i][j];
}

template<class T>
void MatrixClass<T>::Print() const
{
    std::cout << "(" << numRows_ << "x";
    std::cout << numCols_ << ") matrix:" << std::endl;
    for (int i=0;i<numRows_;++i)
    {
        std::cout << std::setprecision(3);
        for (int j=0;j<numCols_;++j)
            std::cout << std::setw(5) << a_[i][j] << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// iterators
template<class T>
typename MatrixClass<T>::RowIt MatrixClass<T>::begin()
{
    return this->a_.begin();
}

template<class T>
typename MatrixClass<T>::RowIt MatrixClass<T>::end()
{
    return this->a_.end();
}

template<class T>
typename MatrixClass<T>::ConstRowIt MatrixClass<T>::begin() const
{
    return this->a_.begin();
}

template<class T>
typename MatrixClass<T>::ConstRowIt MatrixClass<T>::end() const
{
    return this->a_.end();
}
