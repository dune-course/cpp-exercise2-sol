#include <cstdlib>
#include <iostream>
#include <complex>
#include <iomanip>
#include <algorithm>

#include "nummatrix.h"

using namespace std;


//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    // define matrix
    //=====================

    // double 4x3
    NumMatrixClass<double> A(4,3);
    double start1 = 2.0;
    for (int i=0; i<A.Rows(); ++i)
      for (int j=0; j<A.Cols(); ++j)
        A(i,j) = start1 + i + j;


	// (1) Iterate over matrix entries using explicit iterator types
	typedef NumMatrixClass<double>::ConstRowIt RIt;
	typedef NumMatrixClass<double>::ConstColIt CIt;
	for(RIt rit = A.begin(); rit != A.end(); ++rit)
	{
	  for(CIt cit = rit->begin(); cit != rit->end(); ++cit)
		{
			std::cout << *cit << " ";
		}
		std::cout << std::endl;
	}

	std::cout << std::endl;

	// (2) Iterate over matrix using types implicitly obtained by 'auto' keyword
	for(auto rit = A.begin(); rit != A.end(); ++rit)
	{
	  for(auto cit = rit->begin(); cit != rit->end(); ++cit)
		{
			std::cout << *cit << " ";
		}
		std::cout << std::endl;
	}

	std::cout << std::endl;

	// (3) Use the range-based for loop for an even more readable code
	// Doesn't this look pretty? :)

	// Make a copy, since we change matrix values in the following code
	NumMatrixClass<double> B(A);
	// Obtain elements by value
	for(auto row : B)
	{
	  for(auto col : row)
	  {
	    std::cout << col << " ";
	    col = 0.8e15; // doesn't do anything to entries in A
	  }
	  std::cout << std::endl;
	}
	std::cout << std::endl;
	for(auto& row : B)
	{
	  for(auto& col : row)
	  {
	    std::cout << col << " ";
	    col = 4.7e11; // changes entries in A
	  }
	  std::cout << std::endl;
	}
	std::cout << std::endl;
	for(auto const & row : B)
	{
	  for(auto const & col : row)
	  {
	    std::cout << col << " ";
	    //col = 4.7e11; // uncommenting this would cause a compiler error
	  }
	  std::cout << std::endl;
	}

	std::cout << std::endl;

	// (4) Use lamda functions instead to implement the double loop
	// Inner lambda function: Print double value
	auto func_col = [] (double val) { std::cout << val << " "; };

	// Outer lambda function: Iterate over entries of row and call inner lambda function
	auto func_row = [&] (std::vector<double>& row) {
		std::for_each(row.begin(), row.end(), func_col); std::cout << std::endl;
	};

	std::for_each( A.begin(), A.end(), func_row);

   return (0);
}
